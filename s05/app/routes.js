const { exchangeRates } = require('../src/util.js');

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data': {} });
	});

	app.get('/rates', (req, res) => {
		return res.send({
			rates: exchangeRates
		});
	});

	app.post('/currency', (req, res) => {
    //if there is no currency name
		if (!req.body.hasOwnProperty('name')) {
      return res.status(400).send({
        'error': 'Bad Request - missing required parameter NAME'
      })
    };

    //if currency is not a string
    if (typeof req.body.name !== 'string') {
      return res.status(400).send({
        'error': 'Bad Request - NAME has to be a STRING'
      })
    };

    //if currency is empty string
    if (req.body.name === '') {
      return res.status(400).send({
        'error': 'Bad Request - NAME cannot be empty'
      })
    };

    //if there is no exchange rate
    if (!req.body.hasOwnProperty('ex')) {
      return res.status(400).send({
        'error': 'Bad Request - missing required parameter EX'
      })
    };

    //if exchange rate is not an object
    if (typeof req.body.ex !== 'object') {
      return res.status(400).send({
        'error': 'Bad Request - EX has to be an OBJECT'
      })
    };

    //if exchange rate is an empty object
    if (Object.keys(req.body.ex).length <= 0) {
      return res.status(400).send({
        'error': 'Bad Request - EX cannot be empty'
      })
    };

    //if there is no alias
    if (!req.body.hasOwnProperty('alias')) {
      return res.status(400).send({
        'error': 'Bad Request - missing required parameter ALIAS'
      })
    };

    //if alias is not a string
    if (typeof req.body.alias !== 'string') {
      return res.status(400).send({
        'error': 'Bad Request - ALIAS has to be a STRING'
      })
    };

    //if alias is empty string
    if (req.body.alias === '') {
      return res.status(400).send({
        'error': 'Bad Request - ALIAS cannot be empty'
      })
    };

    //if alias is already existing
    if (req.body.alias in exchangeRates) {
      return res.status(400).send({
        'error': 'Bad Request - ALIAS cannot be empty'
      })
    };

    //default case if complete details
    //returns status 200 and submitted object if all requirements are satisfied
    return res.status(200).send({
      data: req.body
    });
	});
};
