const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', () => {
	it('test_api_get_rates_is_running', () => {
		chai.request('http://localhost:5001')
    .get('/rates')
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
		});
	});
	
	it('test_api_get_rates_returns_200', (done) => {
		chai.request('http://localhost:5001')
		.get('/rates')
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();	
		});		
	});
	
	it('test_api_get_rates_returns_object_of_size_5', (done) => {
		chai.request('http://localhost:5001')
		.get('/rates')
		.end((err, res) => {
			expect(Object.keys(res.body.rates)).does.have.length(5);
			done();	
		});
	});

  it('test_api_post_currency_is_200', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
    .type('json')
    .send({
      'alias': 'eur',
      'name': 'Euro',
      'ex': {
        'usd': 1.1,
        'peso': 61.99,
        'won': 1458.29,
        'yen': 158.84,
        'yuan': 7.91
      }
    })
		.end((err, res) => {
      expect(res.status).to.not.equal(undefined);
      done();
		});
	});

  it('test_api_post_currency_returns_400_if_no_currency_name', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
    .type('json')
    .send({
      'alias': 'eur',
      'ex': {
        'usd': 1.1,
        'peso': 61.99,
        'won': 1458.29,
        'yen': 158.84,
        'yuan': 7.91
      }
    })
		.end((err, res) => {
      expect(res.status).to.equal(400)
      done();
		});
	});

  it('test_api_post_currency_returns_400_if_currency_name_is_not_a_string', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
    .type('json')
    .send({
      'alias': 'eur',
      'name': 23,
      'ex': {
        'usd': 1.1,
        'peso': 61.99,
        'won': 1458.29,
        'yen': 158.84,
        'yuan': 7.91
      }
    })
		.end((err, res) => {
      expect(res.status).to.equal(400)
      done();
		});
	});

  it('test_api_post_currency_returns_400_if_currency_name_is_an_empty_string', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
    .type('json')
    .send({
      'alias': 'eur',
      'name': '',
      'ex': {
        'usd': 1.1,
        'peso': 61.99,
        'won': 1458.29,
        'yen': 158.84,
        'yuan': 7.91
      }
    })
		.end((err, res) => {
      expect(res.status).to.equal(400)
      done();
		});
	});

  it('test_api_post_currency_returns_400_if_no_currency_ex', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
    .send({
      'alias': 'eur',
      'name': 'Euro'
    })
		.end((err, res) => {
      expect(res.status).to.equal(400)
      done();
		});
	});

  it('test_api_post_currency_returns_400_if_currency_ex_not_an_object', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
    .type('json')
    .send({
      'alias': 'eur',
      'name': 'Euro',
      'ex': 1.1
    })
		.end((err, res) => {
      expect(res.status).to.equal(400)
      done();
		});
	});

  it('test_api_post_currency_returns_400_if_no_ex_content', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
    .type('json')
    .send({
      'alias': 'eur',
      'name': 'Euro',
      'ex': {
      }
    })
		.end((err, res) => {
      expect(res.status).to.equal(400)
      done();
		});
	});

  it('test_api_post_currency_returns_400_if_no_currency_alias', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
    .type('json')
    .send({
      'name': 'Euro',
      'ex': {
        'usd': 1.1,
        'peso': 61.99,
        'won': 1458.29,
        'yen': 158.84,
        'yuan': 7.91
      }
    })
		.end((err, res) => {
      expect(res.status).to.equal(400);
      done();
		});
	});

  it('test_api_post_currency_returns_400_if_currency_alias_not_a_string', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
    .type('json')
    .send({
      'alias': 23,
      'name': 'Euro',
      'ex': {
        'usd': 1.1,
        'peso': 61.99,
        'won': 1458.29,
        'yen': 158.84,
        'yuan': 7.91
      }
    })
		.end((err, res) => {
      expect(res.status).to.equal(400);
      done();
		});
	});

  it('test_api_post_currency_returns_400_if_currency_alias_is_an_empty_string', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
    .type('json')
    .send({
      'alias': '',
      'name': 'Euro',
      'ex': {
        'usd': 1.1,
        'peso': 61.99,
        'won': 1458.29,
        'yen': 158.84,
        'yuan': 7.91
      }
    })
		.end((err, res) => {
      expect(res.status).to.equal(400);
      done();
		});
	});

  it('test_api_post_currency_returns_400_if_duplicate_alias_found', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
    .type('json')
    .send({
      'alias': 'usd',
      'name': 'United States Dollar',
      'ex': {
        'peso': 50.73,
        'won': 1187.24,
        'yen': 108.63,
        'yuan': 7.03
      }
    })
		.end((err, res) => {
      expect(res.status).to.equal(400);
      done();
		});
	});

  it('test_api_post_currency_returns_200_if_complete_input_given', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
    .type('json')
    .send({
      'alias': 'eur',
      'name': 'Euro',
      'ex': {
        'usd': 1.1,
        'peso': 61.99,
        'won': 1458.29,
        'yen': 158.84,
        'yuan': 7.91
      }
    })
		.end((err, res) => {
      expect(res.status).to.equal(200);
      done();
		});
	});

  it('test_api_post_currency_returns_submitted_object_to_show_submission_was_written', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
    .type('json')
    .send({
      'alias': 'eur',
      'name': 'Euro',
      'ex': {
        'usd': 1.1,
        'peso': 61.99,
        'won': 1458.29,
        'yen': 158.84,
        'yuan': 7.91
      }
    })
		.end((err, res) => {
      expect(res.body.data).to.deep.equal({
        'alias': 'eur',
        'name': 'Euro',
        'ex': {
          'usd': 1.1,
          'peso': 61.99,
          'won': 1458.29,
          'yen': 158.84,
          'yuan': 7.91
        }
      });
      done();
		});
	});
});

