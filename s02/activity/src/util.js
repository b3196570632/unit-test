function factorial(n){
  if (typeof n !== 'number') return undefined;
  if (n < 0) return undefined;
  if (n === 0) return 1;
  if (n === 1) return 1;
  return n * factorial(n - 1);
};

function div_check(x, y){
  if (typeof x !== 'number') return undefined;
  if (typeof y !== 'number') return undefined;
  return (x % y) === 0 ? true : false;
}

module.exports = {
  factorial: factorial,
  div_check: div_check
};