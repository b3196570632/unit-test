const { factorial } = require('../src/util.js');
const { expect, assert } = require('chai');

it('test_fun_factorial_5!_is_120_revised', () => {
  const product = factorial(5);
  expect(product).to.equal(120);
});

it('test_fun_factorial_1!_is_1', () => {
  const product = factorial(1);
  assert.equal(product, 1);
});

it('test_fun_factorial_1!_is_1_expect', () => {
  const product = factorial(1);
  expect(product).to.equal(1);
});

it('test_fun_factorial_-1_is_undefined', () => {
  const product = factorial(-1);
  assert.equal(product, undefined);
});

describe('test_fun_factorials', () => {
  it('test_fun_factorial_5!_is_120', () => {
    const product = factorial(5);
    expect(product).to.equal(120);
  });

  it('test_fun_factorial_n!_is_string', () => {
    const product = factorial('n');
    assert.equal(product, undefined);
  });
});