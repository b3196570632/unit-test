function factorial(n){
  if (typeof n !== 'number') return undefined; // Is to check if the value is number
  if (n < 0) return undefined; // Is to check if the value should be more than zero
  if (n === 0) return 1; // Is to check if the value is 0 return 1
  if (n === 1) return 1; // Is to check if the value is 1 return 1
  return n * factorial(n - 1);
};

module.exports = {
  factorial: factorial
};