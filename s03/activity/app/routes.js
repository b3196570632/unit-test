const { names } = require('../src/util.js');

module.exports = (app) => {
  app.get('/', (req, res) => {
    return res.status(200).send({
      data: {}
    });
  });

  app.get('/people', (req, res) => {
    return res.status(200).send({
      people: names
    });
  });

  app.post('/person', (req, res) => {
    if (!req.body.hasOwnProperty('name')) {
      return res.status(400).send({
        'error': 'Bad Request - missing required parameter NAME'
      })
    };

    if (typeof req.body.name !== 'string') {
      return res.status(400).send({
        'error': 'Bad Request - Name has to be a string'
      })
    };

    if (!req.body.hasOwnProperty('age')) {
      return res.status(400).send({
        'error': 'Bad Request - missing required parameter AGE'
      })
    };

    if (typeof req.body.age !== 'number') {
      return res.status(400).send({
        'error': 'Bad Request - Age has to be a number'
      })
    };

    if (!req.body.hasOwnProperty('username')) {
      return res.status(400).send({
        'error': 'Bad Request - missing required parameter USERNAME'
      })
    };

    return res.status(200).send({});
  });
}