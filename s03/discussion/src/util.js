const names = {
  "Brandon": {
    "name": "Brandon Boyd",
    "age": 35
  },
  "Steve": {
    "name": "Steve Tyler",
    "age": 56
  },
}

module.exports = {
  names: names
}