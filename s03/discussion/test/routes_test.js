const chai = require('chai');
const { expect } = require('chai');

const http = require('chai-http');
chai.use(http);

describe('api_test_suite', () => {
  it("test_api_get_people_is_running", () => {
    chai.request('http://localhost:5001').get('/people')
    .end((err, res) => {
      expect(res).to.not.equal(undefined);
      // This is an assertation using Chai's "expect"
      // It will check if the res object is not equal to "undefined"
    });
  });

  it("test_api_get_people_returns_200", (done) => {
    chai.request('http://localhost:5001').get('/people')
    .end((err, res) => {
      expect(res.status).to.equal(200);
      // This is an assertation that checks if the response is OK
      // 200 means 'OK'
      done();
      // Call done() is used to indicate that the asynchronous test is complete
    });
  });

  it("test_api_post_person_returns_400_if_no_person_name", (done) => {
    chai.request('http://localhost:5001').post('/person')
    .type('json')
    .send({
      alias: "James",
      age: 28
    })
    .end((err, res) => {
      expect(res.status).to.equal(400);
      done();
    });
  });

  it("test_api_post_person_returns_400_if_age_is_string", (done) => {
    chai.request('http://localhost:5001').post('/person')
    .type('json')
    .send({
      name: "James",
      age: "Twenty-Eight"
    })
    .end((err, res) => {
      expect(res.status).to.equal(400);
      done();
    });
  });
});